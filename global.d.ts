interface DBCharacter {
  u: number
  n: string
  w: string
  d?: string
  t: number
  i: string
  s?: string
  in?: string
  k?: string
  b?: string[]
}

declare global {
}
export {
  DBCharacter,
}
